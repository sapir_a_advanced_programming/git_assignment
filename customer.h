#pragma once
#include "Item.h"
#include <set>

class Customer
{
public:
	Customer(string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item it);//add item to the set
	void removeItem(Item it);//remove item from the set

						  //get and set functions
	//getters
	string getName();
	set<Item> getItems();

	//setters
	void setName(string name);
	void setItems(set <Item> items);

private:
	void showCustomerItems(set<Item> i);
	string _name;
	set<Item> _items;


};
