#include "customer.h"

Customer::Customer(string name) //c'tor
{
	_name = name;
}

Customer::Customer() //c'tor
{
}

Customer::~Customer() //d'tor
{
}

double Customer::totalSum() const // returning the total sum for payment of each customer
{
	double sum = 0;
	int count = 0;
	set<Item>::iterator it;
	for (it = _items.begin(); it != _items.end(); it++)
	{
		sum = sum + it->totalPrice();
	}
	return sum;
}

/*
The function adding an item to the list.
Input:
	it = the new item we want to add to the list
Output:
	none
*/
void Customer::addItem(Item it)
{
	int counter = 0;
	set<Item>::iterator setIt;
	setIt = _items.find(it);
	bool exists = _items.find(it) != _items.end();
	if (exists)
	{
		counter = setIt->getCount(); // get currect count
		_items.erase(it); // remove old product
		it.setCount(counter + 1); // update count
		_items.insert(it); // insert updated product
	}
	else
	{
		_items.insert(it); // insert the product
	}
	cout << "Your List Is: " << endl;
	showCustomerItems(_items);
}

/*
The function removes an item from the list.
Input:
	it = the new item we want to add to the list
Output:
	none
*/
void Customer::removeItem(Item it)
{
	int count = 0;
	set<Item>::iterator found_It;
	Item a("", "", 0);

	if ((found_It = _items.find(it)) != _items.end())
	{
		a = *found_It;
		_items.erase(it);
		a.setCount(a.getCount() - 1);
		if (a.getCount() != 0)
		{
			_items.insert(a);
		}
	}
	cout << "Your List Is: " << endl;
	showCustomerItems(_items);
}

string Customer::getName() // returning the item's name
{
	return _name;
}

set<Item> Customer::getItems() // returning the set of items (the list)
{
	return _items;
}

void Customer::setName(string name)  //returning the customer's name
{
	this->_name = name;
}

void Customer::setItems(set<Item> items) // set the Items in set 
{
	for (auto it = items.begin(); it != items.end(); it++)
	{
		_items.insert(*it);
	}

}

void Customer::showCustomerItems(set<Item> i) // prints all the customer's items
{
	cout << "######### " << endl;
	set<Item>::iterator it;

	for (it = i.begin(); it != i.end(); it++)
	{
		std::cout << "item name:  " << it->getName() << " > " << "item serialnumber: " << it->getSerialNumber() << " > " << "item count: " << it->getCount() << " > " << "item unit price: " << it->getUnitPrice() << endl << "######### " << endl;
	}
}
