#include "item.h"

Item::Item(string name , string serialNumber, double unitPrice)//c'tor
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1; //default num of items is 1
}

Item::~Item() //d'tor
{
}

double Item::totalPrice() const // returning the price of an item (concidering the fact that there can be more than one items from each kind)
{
	return (this->_count * this->_unitPrice);
}

bool Item::operator<(const Item & other) const // operator <, returning true or false - how is bigger?
{
	return (this->_serialNumber < other._serialNumber);
}

bool Item::operator>(const Item & other) const // operator >, returning true or false - how is bigger?
{
	return (this->_serialNumber > other._serialNumber);
}

bool Item::operator==(const Item & other) const // operator ==, returning true or false - if the 2 values are the same?
{
	return (this->_serialNumber == other._serialNumber);
}

string Item::getName() const // returning the item's name
{
	return this->_name;
}

string Item::getSerialNumber() const // returning the item's serial number
{
	return this->_serialNumber;
}

int Item::getCount() const // returning the number of times that the customer bought the item
{
	return this->_count;
}

double Item::getUnitPrice() const //returning the price of a specific item
{
	return _unitPrice;
}

void Item::setName(string name) // setting the item's name
{
	this->_name = name;
}

void Item::setSerialNumber(string serialNumber) //setting the item's serial number
{
	this->_serialNumber = serialNumber;
}

void Item::setCount(int count) //setting the number of times that the customer bought the item
{
	_count = count;
}

void Item::setUnitPrice(double unitPrice) //setting the price for the item
{
	this->_unitPrice = unitPrice;
}