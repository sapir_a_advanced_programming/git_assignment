#include"Customer.h"
#include<map>

#define SIZE 10
#define END_ADD_REMOVE 0
#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4

void printOptions();
void printAvailableItems(Item itemlist[]); // print out items
void addItemToCustomerList(Customer* customer, Item itemlist[]); // adding an item to 
void removeItemFromCustomerList(Customer* cus, Item itemlist[]);// controll the remove items
void signAndBuy(map<string, Customer>* abcCustomers, Item itemlist[]); // option 1 in the menu
void knownCustomer(map<string, Customer>* abcCustomers, Item itemlist[]); // option 2 in the menu
void findMaxTotal(map<string, Customer> abcCustomers); //option 3 in the menu

int main()
{
	int userChoice = 0;
	map<string, Customer> abcCustomers;
	Item itemList[SIZE] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	std::cout << "Welcome to MagshiMart!" << endl;
	while (userChoice != FOUR)
	{
		printOptions();
		std::cin >> userChoice;
		switch (userChoice)
		{
		case ONE:
			signAndBuy(&abcCustomers, itemList);
			break;
		case TWO:
			knownCustomer(&abcCustomers, itemList);
			break;
		case THREE:
			findMaxTotal(abcCustomers);
			break;
		default:
			std::cout << "Invaild choice try again! " << std::endl;
			break;
		}
	}
	system("pause");
	return 0;
}

/*
The function prints the menu.
Input:
	none
Output:
	none
*/
void printOptions()
{
	std::cout << "1. to sign as customer and buy items \n2. to uptade existing customer's items \n3. to print the customer who pays the most \n4. to exit" << endl;
}

/*
The function prints the items that the customer can buy.
Input:
	none
Output:
	none
*/
void printAvailableItems(Item itemlist[])
{
	for (int i = 0; i < SIZE; i++)
	{
		std::cout << itemlist[i].getSerialNumber() << " - " << itemlist[i].getName() << "  price: " << itemlist[i].getUnitPrice() << endl;
	}
}

/*
The function adds an item to the customer's list, while he doesn't picks '0'
Input:
	customer = the customer we want to add to his list a new item
	itemList = the list of the available items
Output:
	none
*/
void addItemToCustomerList(Customer* customer, Item itemlist[])
{
	string choice = "";
	while (choice.compare("0") != END_ADD_REMOVE)
	{
		std::cout << "What item would you like to buy ?" << std::endl << "Name Of Item: ";
		std::cin >> choice;
		for (int i = 0; i < SIZE; ++i)
		{
			if ((stoi(itemlist[i].getSerialNumber())) == stoi(choice))
			{
				customer->addItem(itemlist[i]);
				break;
			}
		}
	}
}

/*
The function removes an item from the customer's list, while he doesn't picks '0'
Input:
	customer = the customer we want to add to his list a new item
	itemList = the list of the available items
Output:
	none
*/
void removeItemFromCustomerList(Customer* customer, Item itemlist[])
{
	string choice = "";
	while (choice.compare("0") != END_ADD_REMOVE)
	{
		std::cout << "What item would you like to remove ?" << std::endl << "Name of Item: ";
		std::cin >> choice;
		for (int i = 0; i < SIZE; i++)
		{
			if (itemlist[i].getName() == choice)
			{
				customer->removeItem(itemlist[i]);
				break;
			}
		}
	}
}

/*
The function of the first option.
The customer signs up and adds items to his lise.
Input:
	abcCustomers = a map of all the customers and their details.
	itemList = the list of the available items
Output:
	none
*/
void signAndBuy(map<string, Customer>* abcCustomers, Item itemlist[])
{
	string customerName;
	std::cout << "Enter new customer name: ";
	std::cin >> customerName;
	Customer new_customer(customerName);
	bool exists = abcCustomers->find(customerName) != abcCustomers->end(); // if this customer exist
	if (!exists)
	{
		(*abcCustomers).insert(pair<string, Customer>(customerName, new_customer));
		std::cout << "The items you can buy are: (0 to exit)" << endl;
		printAvailableItems(itemlist);
		addItemToCustomerList(&(*abcCustomers)[customerName], itemlist);
	}
	else
	{
		std::cout << "This customer already exist try again later " << endl;
	}
}

/*
The function of the second option.
The customer can change his details and items.
Input:
	abcCustomers = a map of all the customers and their details.
	itemList = the list of the available items
Output:
	none
*/
void knownCustomer(map<string, Customer>* abcCustomers, Item itemlist[])
{
	string customerName = "";
	int userChoice = 0;
	map<string, Customer>::iterator it;
	std::cout << "Customer name: ";
	std::cin >> customerName;
	bool exists = abcCustomers->find(customerName) != abcCustomers->end(); // if the customer exists
	if (exists)
	{
		std::cout << "\n1. add item \n2. remove item \n3. back to menu" << endl;
		std::cin >> userChoice;
		while (userChoice != THREE)
		{
			switch (userChoice)
			{
			case ONE:
				std::cout << "The items you can buy are: (0 to exit)" << endl;
				printAvailableItems(itemlist);
				addItemToCustomerList(&(*abcCustomers)[customerName], itemlist);
				std::cout << "\n1. add item \n2. remove item \n3. back to menu" << endl;
				std::cin >> userChoice;
				break;
			case TWO:
				printAvailableItems(itemlist);
				removeItemFromCustomerList(&(*abcCustomers)[customerName], itemlist);
				std::cout << "\n1. add item \n2. remove item \n3. back to menu" << endl;
				std::cin >> userChoice;
				break;
			default:
				break;
			}
		}
	}
	else
	{
		std::cout << "the customer not found! " << endl;
	}
}

/*
The function of the third option.
The function checks who spent the biggest total price, tnd prints his name and the amount of money he spent.
Input:
	abcCustomers = a map of all the customers and their details.
Output:
	none
*/
void findMaxTotal(map<string, Customer> abcCustomers)
{
	map<double, string> customer;
	map<double, string>::iterator max;
	for (auto it = abcCustomers.begin(); it != abcCustomers.end(); it++)
	{
		customer.insert(pair<double, string>(it->second.totalSum(), it->first));
	}

	for (auto it1 = customer.begin(); it1 != customer.end(); it1++)
	{
		if (next(it1) != customer.end())
		{
			if ((it1->first) > ((++it1)->first))
			{
				max = it1;
			}
			else
			{
				max = ++it1;
			}
		}
		else
		{
			max = it1;
		}
		
	}
	std::cout << "The customer that his totl price is the biggest is: " << endl;
	std::cout << max->first << " - " << max->second << endl;
}